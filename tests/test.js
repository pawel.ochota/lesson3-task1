const modifyEverySecondElement = require('..');

describe('modifyEverySecondElement', () => {
  it('should returns an array', async () => {
    const result = modifyEverySecondElement([1, 1]);

    expect(Array.isArray(result)).toBeTruthy();
  });

  it('should modify original parameter', async () => {
    const array = [1, 2, 3];
    const arrayCopy = [...array];
    modifyEverySecondElement(array);
  
    expect(array).not.toEqual(arrayCopy);
  });

  it('should returns an array which has every second element incremented by 1', async () => {
    expect(modifyEverySecondElement([1, 2, 3])).toEqual([1, 3, 3]);
    expect(modifyEverySecondElement([10, 10, 10, 10])).toEqual([10, 11, 10, 11]);
    expect(modifyEverySecondElement([0, 1, 0, 1, 0, 1])).toEqual([0, 2, 0, 2, 0, 2]);
  });
});
